package com.offcn.bigdata.spark.p4

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * 分组排序，按照某个组别，在组别内进行排序，得到结果
  */
object _03GroupedSortOps {
    def main(args: Array[String]): Unit = {
        val conf = new SparkConf()
            .setAppName(s"${_03GroupedSortOps.getClass.getSimpleName}")
            .setMaster("local[*]")
        val sc = new SparkContext(conf)

        val list = sc.parallelize(List(
            "chinese ls 91",
            "english ww 56",
            "chinese zs 90",
            "chinese zl 76",
            "english zq 88",
            "chinese wb 95",
            "chinese sj 74",
            "english ts 87",
            "english ys 67",
            "english mz 77",
            "chinese yj 98",
            "english gk 96"
        ))
        val scores:RDD[Score] = list.map(line => {
            val fields = line.split("\\s+")
            if(fields == null || fields.length != 3) {
                Score(null, null, -1)
            } else {
                Score(fields(0), fields(1), fields(2).toInt)
            }
        }).filter(score => score.name != null)

        //求出每个科目中成绩排名前三的信息
        val course2Score: RDD[(String, Score)] = scores.map(score => (score.course, score))
        val course2Scores:RDD[(String, Iterable[Score])] = course2Score.groupByKey()
//        course2Scores.foreach(println)
        //分组之后进行组内排序
        course2Scores.map{case (course, scores) => {
            val ret = scores.toList.sortWith((s1, s2) => s1.score > s2.score).take(3)
            (course, ret)
        }}.foreach(println)

        sc.stop()
    }
}
case class Score(course: String, name: String, score: Int)