package com.offcn.bigdata.spark.p3

import org.apache.spark.{SparkConf, SparkContext}

/*
    累加器
        其作用就相当于计数器
 */
object _04AccumulatorOps {
    def main(args: Array[String]): Unit = {
        val conf = new SparkConf()
            .setAppName(s"${_04AccumulatorOps.getClass.getSimpleName}")
            .setMaster("local[1]")

        val sc = new SparkContext(conf)

        val list = sc.parallelize(List(
            "a is abstraction in spark is",
            "shared spark that abstraction be is in spark abstraction"
        ))
        var sparkCount = 0
        val ret = list.flatMap(_.split("\\s+")).map(word => {
            if(word == "spark") {
                sparkCount += 1
            }
            (word, 1)
        }).reduceByKey(_+_)

        ret.foreach(println)

        println("=====sparkCount: " + sparkCount)

        sc.stop()
    }
}
