package com.offcn

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object GroupByOps {
    def main(args: Array[String]): Unit = {
        case class Student(id: Int, name: String, province: String)
        val list = List(
            Student(1, "刘博", "天津"),
            Student(10, "王鑫达", "天津"),
            Student(2, "霍龙飞", "山西"),
            Student(3, "付云瑾", "山东"),
            Student(4, "何浩", "湖南"),
            Student(10086, "刘武", "湖南"),
            Student(5, "龙韬", "湖南"),
            Student(6, "范帅", "湖南"),
            Student(10087, "成思远", "山西"),
            Student(7, "孟阳阳", "山西"),
            Student(8, "吴延俊", "山东"),
            Student(10089, "小岚岚", "山东")
        )

        val map = mutable.Map[String, ArrayBuffer[Student]]()
        for(stu <- list) {
            val abOption = map.get(stu.province)
            if(abOption.isDefined) {
                val ab = abOption.get
                ab.append(stu)
            } else {
                val newAb = new ArrayBuffer[Student]()
                newAb.append(stu)
                map.put(stu.province, newAb)
            }
        }

        map.foreach{case (province, ab) => {
            println(s"${province} --> ${ab}")
        }}
    }
}
