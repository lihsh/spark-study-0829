package com.offcn;

import java.io.*;

/**
 * 作业：
 *    transient volatile native关键字学习
 */
public class ObjectSerOps {
    public static void main(String[] args) throws Exception {
        writeObj();
        readObj();
    }

    private static void readObj() throws Exception {
        FileInputStream fis = new FileInputStream("out/obj.txt");
        ObjectInputStream ois = new ObjectInputStream(fis);

        Object obj = ois.readObject();

        Person[] ps = (Person[])obj;
        for(Person p : ps) {
            System.out.println(p);
        }
        ois.close();
    }

    private static void writeObj() throws Exception {
        FileOutputStream fos = new FileOutputStream("out/obj.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        Person[] ps = {
                new Person("付云瑾", 18),
                new Person("霍龙飞", 16),
                new Person("刘卓凡", 19)
        };
        oos.writeObject(ps);

        oos.close();
    }
}
class Person implements Serializable{
    private String name;
    private int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }
}
