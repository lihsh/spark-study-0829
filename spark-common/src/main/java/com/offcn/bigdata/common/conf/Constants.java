package com.offcn.bigdata.common.conf;

public interface Constants {

    String JDBC_URL = "jdbc.url";

    String JDBC_DRIVER = "jdbc.driver";
    String JDBC_USERNAME = "jdbc.username";
    String JDBC_PASSWORD = "jdbc.password";
}
