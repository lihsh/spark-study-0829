package com.offcn.bigdata.spark.domain;

import java.lang.reflect.Field;

public class Student {
    private String name;
    private int age;
    private String gender;
    private String province;

    public Student(String name, int age, String gender, String province) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.province = province;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public static void main(String[] args) {
        Class<Student> clazz = Student.class;

        Field[] fields = clazz.getDeclaredFields();
        for(Field field : fields) {
            Class<?> type = field.getType();
            String name = field.getName();
            System.out.println(name + "----" + type);
        }
    }
}
