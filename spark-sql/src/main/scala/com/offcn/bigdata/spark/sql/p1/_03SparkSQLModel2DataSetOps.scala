package com.offcn.bigdata.spark.sql.p1

import org.apache.spark.sql.SparkSession

/**
  * SparkSQL编程模型的构建
  *     DataFrame和DataSet
  *  1. 基于反射的方式构建
  *  2. 动态编程的方式进行构建
  */
object _03SparkSQLModel2DataSetOps {
    def main(args: Array[String]): Unit = {
        val spark = SparkSession.builder()
                        .appName("_03SparkSQLModel2DataSetOps")
                        .master("local[*]")
                        .getOrCreate()
        import spark.implicits._
        val stus = spark.sparkContext.parallelize(List(
            Person("车传广", 23, "男", "辽宁"),
            Person("闫逾恒", 22, "男", "河北"),
            Person("刘博", 20, "男", "天津"),
            Person("王鑫达", 23, "男", "浙江"),
            Person("田志", 23, "男", "河南")
        ))
        val ds = spark.createDataset(stus)

        ds.printSchema()
        ds.show
        spark.stop()
    }
}


case class Person(val name: String, val age: Int, val gender: String, val province: String)
