package com.offcn.bigdata;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;

public class Test {
    public static void main(String[] args) throws Exception {
        FileSystem fs = FileSystem.get(new URI("hdfs://bigdata01:9000/"), new Configuration());

        FSDataOutputStream fdos = fs.create(new Path("hdfs://bigdata01:9000/data/spark/monitor/hehe2.log"));
        byte[] bytes = "hello handsome boy\nhello cute little baby\n".getBytes();
        fdos.write(bytes);

        fdos.close();
        fs.close();
    }

    private static void write2LocalFile() throws IOException {
        BufferedWriter bw = new BufferedWriter(new FileWriter("E:/data/minitor/heihei.txt"));

        bw.write("hello handsome boy");
        bw.newLine();
        bw.write("hello cute little baby");
        bw.newLine();
        bw.close();
    }
}
