create database test_0829;

create table `test_0829`.`teacher_info` (
    name string,
    height double
) row format delimited
fields terminated by ',';


create table `test_0829`.`teacher_basic` (
    name string,
    age int,
    gender boolean,
    course int
) row format delimited
fields terminated by ',';


load data local inpath '/home/bigdata/jars/spark/sql/teacher_info.txt' into table `test_0829`.`teacher_info`;

load data local inpath '/home/bigdata/jars/spark/sql/teacher_basic.txt' into table `test_0829`.`teacher_basic`;



create table `test_0829`.`teacher` as select
  i.name,
  b.age,
  if(b.gender, 'female', 'male'),
  i.height,
  b.course
from `test_0829`.`teacher_info` i
inner join `test_0829`.`teacher_basic` b on i.name = b.name;

