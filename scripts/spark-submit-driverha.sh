
SPARK_HOME=/home/bigdata/apps/spark

${SPARK_HOME}/bin/spark-submit \
--master spark://bigdata01:7077 \
--deploy-mode cluster \
--supervise \
--class com.offcn.bigdata.streaming.p2._06DriverHA2UpdateStateByKeyOps \
--driver-memory 600m \
--driver-cores 1 \
--executor-memory 600m \
--executor-cores 2 \
--total-executor-cores 2 \
hdfs://bigdata01:9000/jars/spark/streaming/spark-driverha.jar \
2 bigdata01 9999 \
hdfs://bigdata01:9000/data/spark/monitor/chk
