
export HADOOP_CONF_DIR=/home/bigdata/app/hadoop/etc/hadoop

SPARK_HOME=/home/bigdata/apps/spark

${SPARK_HOME}/bin/spark-submit \
--master yarn \
--deploy-mode cluster \
--class com.offcn.bigdata.spark.p1.RemoteScalaSparkWordCountApp \
--driver-memory 600m \
--driver-cores 1 \
--executor-memory 600m \
--executor-cores 1 \
--num-executors 1 \
hdfs://ns1/jars/spark/core/spark-wc.jar \
hdfs://ns1/home/bigdata/hello.log
