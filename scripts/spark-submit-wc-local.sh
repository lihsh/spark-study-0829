
SPARK_HOME=/home/bigdata/apps/spark

${SPARK_HOME}/bin/spark-submit \
--master local[*] \
--class com.offcn.bigdata.spark.p1.RemoteScalaSparkWordCountApp \
--driver-memory 600m \
--executor-memory 600m \
/home/bigdata/jars/spark/core/spark-wc.jar \
hdfs://ns1/home/bigdata/hello.log

