
SPARK_HOME=/home/bigdata/apps/spark

${SPARK_HOME}/bin/spark-submit \
--master spark://bigdata02:7077 \
--deploy-mode client \
--class com.offcn.bigdata.spark.sql._06SparkSQLIntegeredWithHiveOps \
--driver-memory 600m \
--executor-memory 600m \
--executor-cores 1 \
--total-executor-cores 1 \
/home/bigdata/jars/spark/sql/spark-sql-1.0-SNAPSHOT-jar-with-dependencies.jar \
hdfs://ns1/data/sparksql/teacher_info.txt \
hdfs://ns1/data/sparksql/teacher_basic.txt

